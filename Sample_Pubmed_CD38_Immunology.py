
'''
These 2 variables need to be modified based on your search:
med_term
simple_med_term

Also put in your Pubmed API and chatgpt API keys below
'''

import requests
from bs4 import BeautifulSoup
import pandas as pd
import time
import openai
import os

def fetch_pubmed_info(url):
    response = requests.get(url)
    if response.status_code == 200:
        soup = BeautifulSoup(response.text, 'html.parser')
        title = soup.find('h1', {'class': 'heading-title'}).text.strip()
        authors = [author.text.strip() for author in soup.find_all('span', {'class': 'authors-list-item'})]
        abstract = soup.find('div', {'class': 'abstract'}).text.strip()
        # The class or tag for date_published might need adjustment based on PubMed's page structure
        date_published = soup.find('span', {'class': 'cit'}).text.strip()
        full_text_links = soup.find('div', {'class': 'full-text-links'})
        full_length_link = None
        if full_text_links:
            full_text_links_list = full_text_links.find('div', {'class': 'full-text-links-list'})
            if full_text_links_list:
                href_value = full_text_links_list.find('a')['href']
                full_length_link = href_value
        return title, authors, abstract, date_published, full_length_link
    return None, None, None, None, None

start_time = time.time()
med_term = '(("CD38"[Title/Abstract] AND "Systemic Lupus Erythematosus"[Title/Abstract]) OR ("CD-38"[Title/Abstract] AND "Systemic Lupus Erythematosus"[Title/Abstract]) OR ("CD-38"[Title/Abstract] AND "Lupus Nephritis"[Title/Abstract]) OR ("CD38"[Title/Abstract] AND "Lupus Nephritis"[Title/Abstract])) OR ("CD-38"[Title/Abstract] AND "Membranous Nephropathy"[Title/Abstract]) OR ("CD38"[Title/Abstract] AND "Membranous Nephropathy"[Title/Abstract]) OR ("CD-38"[Title/Abstract] AND "Sjogren"[Title/Abstract]) OR ("CD38"[Title/Abstract] AND Sjogren"[Title/Abstract]))'

# '(("CD38"[Title/Abstract] AND "autoimmune"[Title/Abstract]) OR ("CD-38"[Title/Abstract] AND "autoimmune"[Title/Abstract]) OR ("CD-38"[Title/Abstract] AND "immunology"[Title/Abstract]) OR ("CD38"[Title/Abstract] AND "immunology"[Title/Abstract]))'
#
simple_med_term = "CD38_SLE_MN_LN_Sjo"
#"CD38_Immunology"
#"CD38_Immunology"
api_key = "Pubmed API Key"
openai.api_key = "ChatGPT API key"
results = []

pubmed_search_url = f"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term={med_term}&usehistory=y&api_key={api_key}"
response = requests.get(pubmed_search_url)
soup = BeautifulSoup(response.content, 'xml')
WebEnv = soup.find('WebEnv').text
QueryKey = soup.find('QueryKey').text

pubmed_fetch_url = f"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&query_key={QueryKey}&WebEnv={WebEnv}&api_key={api_key}&retmode=xml"
response = requests.get(pubmed_fetch_url)
soup = BeautifulSoup(response.content, 'xml')

for article in soup.find_all('PubmedArticle'):
    pmid = article.find('PMID').text
    url = f"https://pubmed.ncbi.nlm.nih.gov/{pmid}"
    abstract = article.find('AbstractText')
    if abstract:
        abstract = abstract.text
        try:
            response = openai.ChatCompletion.create(
                model="gpt-3.5-turbo",
                messages=[
                    {"role": "system",
                     "content": "You are a medical literature expert! Summarize the abstract in less than 60 words."},
                    {"role": "user", "content": abstract}
                ]
            )
            abstract_summary = response.choices[0].message.content
        except Exception as e:
            abstract_summary = f"Error: {str(e)}"

        title, authors, abstract, date_published, full_length_link = fetch_pubmed_info(url)

        results.append({
            "URL": url,
            "Date Published": date_published,
            "Abstract ID": pmid,
            "Abstract Summary": abstract_summary,
            "Full Length Paper Link": full_length_link or "Not available"
        })

df = pd.DataFrame(results)
excel_file_name = f"Abstract_search_report_{simple_med_term}.xlsx"
df.to_excel(excel_file_name, index=False)
print(f"Results have been saved to '{excel_file_name}'.")

end_time = time.time()
execution_time = (end_time - start_time) / 60
print(f"\nThe script executed in {execution_time} minutes.")
